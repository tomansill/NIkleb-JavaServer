package com.tomansill.nikleb;


/*#################### INNER CLASS ####################*/

import java.lang.reflect.Method;

class ResourceFunction{

	private final String resource_string;

	private final Class clazz;

	private final Method method;

	private final String[] default_values;

	private final String[] parameter_names;

	ResourceFunction(
			final String resource_string,
			final Class clazz,
			final Method method,
			final String[] default_values,
			final String[] parameter_names
	){
		this.resource_string = resource_string;
		this.clazz = clazz;
		this.method = method;
		this.default_values = default_values;
		this.parameter_names = parameter_names;
	}

	public String getResourceString(){
		return this.resource_string;
	}

	public Class getClazz(){
		return this.clazz;
	}

	public Method getMethod(){
		return this.method;
	}

	public String[] getDefaultValues(){
		return this.default_values;
	}

	public String[] getParameterNames(){
		return this.parameter_names;
	}
}
