package com.tomansill.nikleb;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tomansill.nikleb.Enum.Type;
import com.tomansill.nikleb.protocol.ProtocolCloseReason;
import com.tomansill.nikleb.protocol.request.HandshakeProtocolRequest;
import com.tomansill.nikleb.protocol.request.NewStreamingProtocolRequest;
import com.tomansill.nikleb.protocol.request.SingleFireProtocolRequest;
import com.tomansill.nikleb.protocol.response.*;
import com.tomansill.nikleb.response.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

/** Connection class */
public class Connection{

	/** Parent */
	private final Nikleb parent;

	/** Client id for this connection */
	private final int client_id;

	/** Flag if the handshake has been established */
	private boolean handshake = false;

	/** id counter lock */
	private Lock id_counter_lock = new ReentrantLock();

	/** Synchronization id */
	private int id_counter = 0;

	/** Flag for id counter overflow */
	private boolean id_counter_overflow = false;

	private final Consumer<ProtocolResponse> sync_send_function;
	private final Consumer<ProtocolResponse> async_send_function;
	private final Consumer<ProtocolCloseReason> close_function;

	/** Streams */
	private Map<Integer,Object> streams = new HashMap<>();

	/** Creates the connection
	 *  @param parent parent manager so connection can contact
	 *  @param client_id client id associated to the connection
	 */
	Connection(
		final Nikleb parent,
		final int client_id,
		final Consumer<ProtocolResponse> sync_send_function,
		final Consumer<ProtocolResponse> async_send_function,
		final Consumer<ProtocolCloseReason> close_function
	){
		this.parent = parent;
		this.client_id = client_id;
		this.sync_send_function = sync_send_function;
		this.async_send_function = async_send_function;
		this.close_function = close_function;
	}

	private boolean isNewIdValid(final int id){

		try{
			// Lock on counter then unlock when it finishes
			this.id_counter_lock.lock();

			// Check if if is valid. If it is not, return false
			if(id != this.id_counter) return false;

			// Count up
			this.id_counter++;

			// Check if id counter has overflowed (this may take a long time to become true)
			if(id == Integer.MAX_VALUE) this.id_counter_overflow = true;

			// Handle when the id_counter has overflown
			// Loop until we get an unused id number
			while(this.id_counter_overflow && this.streams.containsKey(this.id_counter)) this.id_counter++;

		}finally{
			this.id_counter_lock.unlock();
		}

		return true;
	}

	public void process(final String message){
		ProtocolResponse response = processMessage(message);
		if(response == null) return;
		this.sync_send_function.accept(response);
		if(response instanceof CloseOnProtocolResponse) this.close_function.accept(((CloseOnProtocolResponse) response).getReason());
	}

	private ProtocolResponse processMessage(final String message){

		// GSON object
		Gson gson = new Gson();

		// Type
		Type type;

		// Try to understand the message
		try{

			// Convert to generic JSON map
			Map<String, Object> generic = gson.fromJson(message, new TypeToken<Map<String, Object>>(){}.getType());

			// Get Type
			type = Type.get((String) generic.get("type"));

			// Check if type exists
			if(type == null){
				System.out.println("Not a valid response");
				return new ErrorProtocolResponse();
			}

		}catch(JsonParseException e){
			System.out.println("Invalid JSON");
			return new ErrorProtocolResponse();
		}

		// Process response
		ProtocolResponse response = this.processRequest(message, type);

		// If null, send error
		if(response == null){
			System.out.println("null response");
			return new ErrorProtocolResponse();
		}

		// If protocol contains NextIdProtocolResponse, attach next id
		if(response instanceof NextIdProtocolResponse){
			try{
				// Lock on counter then unlock when it finishes
				this.id_counter_lock.lock();

				// Assign next id
				((NextIdProtocolResponse)response).setNextId(this.id_counter);

			}finally{
				this.id_counter_lock.unlock();
			}
		}

		// Return
		return response;
	}

	private ProtocolResponse processRequest(final String message, final Type type){

		System.out.println("Connection.process(" + message + ")");

		Gson gson = new Gson();

		// Try block to catch any bad errors
		try{

			// Switch on type
			switch(type){

				// Handshake message
				case HANDSHAKE:

					if(handshake){
						// Handshaked twice, the protocol is desynchronized
						return new ErrorProtocolResponse();
					}

					// Attempt to convert to object
					HandshakeProtocolRequest hr = gson.fromJson(message, HandshakeProtocolRequest.class);

					// Check if this server supports this client's protocol version
					if(!Nikleb.getSupportedProtocolVersions().contains(hr.getProtocolVersion())){
						new HandshakeFailure();
					}

					// Set handshake flag
					this.handshake = true;

					// TODO do with options

					// Return success
					return new HandshakeSuccessProtocolResponse();

				// Single-Fire ProtocolRequest
				case REQUEST_SINGLE:

					// Check handshake
					if(!handshake) return null; //TODO

					// Attempt to convert to object
					SingleFireProtocolRequest sfpr = new Gson().fromJson(message, SingleFireProtocolRequest.class);

					// Check the id - must be new and unique
					if(!this.isNewIdValid(sfpr.getId())) return new SynchronizationProtocolResponse();

					// Process response
					Response response = this.parent.serviceRequest(sfpr);

					return null;

				// Streaming ProtocolRequest
				case REQUEST_STREAM:

					// Check handshake
					if(!handshake) return null; //TODO

					// Attempt to convert to object
					NewStreamingProtocolRequest nspr = new Gson().fromJson(message, NewStreamingProtocolRequest.class);

					return null;

				// Streaming Update ProtocolRequest
				case REQUEST_STREAM_UPDATE:

					// Check handshake
					if(!handshake) return null; //TODO

					// Attempt to convert to object
					//NewStreamingProtocolRequest sr = new Gson().fromJson(message, NewStreamingProtocolRequest.class);

					return null;

				// Streaming Settings Update ProtocolRequest
				case REQUEST_STREAM_SETTINGS:

					// Check handshake
					if(!handshake) return null; //TODO

					// Attempt to convert to object
					//NewStreamingProtocolRequest sr = new Gson().fromJson(message, NewStreamingProtocolRequest.class);

					return null;

				// Streaming Termination ProtocolRequest
				case REQUEST_STREAM_TERMINATE:

					// Check handshake
					if(!handshake) return null; //TODO

					// Attempt to convert to object
					//NewStreamingProtocolRequest sr = new Gson().fromJson(message, NewStreamingProtocolRequest.class);

					return null;

				// Catch-all for invalid types
				default: return null;
			}
		}catch(JsonParseException e){
			System.out.println("It's gibberish.");
			return new ErrorProtocolResponse();
		}catch(Exception e){
			e.printStackTrace();
			return new ErrorProtocolResponse();
		}
	}

	/** Closes the connection */
	public void close(){

		// Do something here like convertToResponse termination message with "NO_MORE_DATA"

		// Destroy the connection
		this.parent.destroyConnection(this.client_id);
	}
}