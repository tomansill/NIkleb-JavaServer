package com.tomansill.nikleb;

import com.tomansill.nikleb.Enum.ParameterErrorType;
import com.tomansill.nikleb.annotation.DefaultValue;
import com.tomansill.nikleb.annotation.Parameter;
import com.tomansill.nikleb.annotation.Resource;
import com.tomansill.nikleb.exception.*;
import com.tomansill.nikleb.protocol.ProtocolCloseReason;
import com.tomansill.nikleb.protocol.request.DataProtocolRequest;
import com.tomansill.nikleb.protocol.request.NewDataProtocolRequest;
import com.tomansill.nikleb.protocol.response.ProtocolResponse;
import com.tomansill.nikleb.response.Response;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

public class Nikleb{

	/*#################### STATIC MEMBERS ####################*/

	/** Semantic Versioning - Major Version */
	private final static int MAJOR_VERSION = 0;

	/** Semantic Versioning - Minor Version */
	private final static int MINOR_VERSION = 0;

	/** Semantic Versioning - Hotfix Version */
	private final static int HOTFIX_VERSION = 0;

	/** List of accepted protocols versions */
	private final static Set<String> SUPPORTED_PROTOCOL_VERSIONS = new HashSet<>();

	/* Initialize SUPPORTED_PROTOCOL_VERSIONS */
	static{
		SUPPORTED_PROTOCOL_VERSIONS.add("0.0");
	}

	/** Returns a set of accepted protocol versions that Nikleb library supports
	 *  @return a set of accepted protocol versions
	 */
	public static Set<String> getSupportedProtocolVersions(){
		return Collections.unmodifiableSet(SUPPORTED_PROTOCOL_VERSIONS);
	}

	/** Returns Nikleb library's semantic version
	 *  @return semantic version
	 */
	public static String getVersion(){
		return MAJOR_VERSION + "." + MINOR_VERSION + "." + HOTFIX_VERSION;
	}

	/*#################### CLASS MEMBERS ####################*/

	/** Map of client and connection */
	private final Map<Integer,Connection> connections = new HashMap<>();

	/** Client id client_id_lock */
	private Lock client_id_lock = new ReentrantLock();

	/** Client id counter */
	private int client_id_counter = Integer.MIN_VALUE;

	/** Flag to indicate that the id counter has overflown */
	private boolean client_id_counter_overflow = false;

	/** Resources */
	private final HashMap<String,ResourceFunction> resources = new HashMap<>();

	/** Default constructor */
	public Nikleb(){}

	/** Creates a new connection
	 *  @return Connection
	 */
	public Connection createConnection(
			Consumer<ProtocolResponse> sync_send_function,
			Consumer<ProtocolResponse> async_send_function,
			Consumer<ProtocolCloseReason> close_function
	){

		// Declare id
		int client_id;


		try{
			// Lock on counter then unlock when it finishes
			client_id_lock.lock();

			// Draw from the counter
			client_id = this.client_id_counter++;

			// Check if client_id_counter has overflowed (this may take a long time to become true)
			if(client_id == Integer.MAX_VALUE) this.client_id_counter_overflow = true;

			// Handle when the client_id_counter has overflown
			// Loop until we get an unused id number
			while(this.client_id_counter_overflow && this.connections.containsKey(client_id)){
				client_id = this.client_id_counter++;
			}
		}finally{
			client_id_lock.unlock();
		}

		// Create a connection
		Connection connection = new Connection(this, client_id, sync_send_function, async_send_function, close_function);

		// Store it in the map
		this.connections.put(client_id, connection);

		// Return the connection
		return connection;
	}

	/** Registers a servlet to the service where it will be immediately be made available to the connections.
	 *  The servlets needs at least contain a @Resource annotation on its methods.
	 *  @param servlet Servlet object
	 *  @throws IllegalArgumentException thrown when the servlet object is null
	 *  @throws ResourceMappingException thrown when the resource mapping is invalid
	 *  @throws ParameterMappingException thrown when the parameter mapping on a resource is invalid
	 *  @throws DuplicateResourceException thrown when a duplicate resource is found.
	 */
	public void registerServlet(final Class servlet){
		this.registerServlet(servlet, false);
	}

	/** Registers a servlet to the service where it will be immediately be made available to the connections.
	 *  The servlets needs at least contain a @Resource annotation on its methods.
	 *  @param servlet Servlet object
	 *  @param overwrite_duplicates true to overwrite any duplicate resources that has been established by previous
	 *                              servlets, false to raise DuplicateResourceException on any duplicate resources
	 *  @throws IllegalArgumentException thrown when the servlet object is null
	 *  @throws ResourceMappingException thrown when the resource mapping is invalid
	 *  @throws ParameterMappingException thrown when the parameter mapping on a resource is invalid
	 *  @throws DuplicateResourceException thrown when a duplicate resource is found.
	 */
	public void registerServlet(final Class servlet, final boolean overwrite_duplicates) throws
		IllegalArgumentException,
		ResourceMappingException,
		ParameterMappingException,
		DuplicateResourceException
	{

		// Check servlet object
		if(servlet == null) throw new IllegalArgumentException("servlet is null");

		// Temporary map
		Map<String,ResourceFunction> temp_resources = new HashMap<>();

		// Scan the object for @Resource annotations
		for(Method method : servlet.getMethods()){

			System.out.println("method: " + method.getName() + "\t annotations: " + Arrays.toString(method.getDeclaredAnnotations()));

			// Get Annotation if it exists
			Resource resource = method.getAnnotation(Resource.class);
			if(resource == null) continue;

			// Get resource string
			String resource_string = resource.value();

			/* Decided to allow empty resource name
			// Check if resource string is valid
			if(resource_string.isEmpty()){

				// Build error message
				StringBuilder sb = new StringBuilder();
				sb.append("The value of @Resource annotation in ");
				sb.append(servlet.getName());
				sb.append('.');
				sb.append(method.getName());
				sb.append(" is empty.");

				// Throw it
				throw new ResourceMappingException(sb.toString());
			}
			*/

			// TODO further check the validity of resource string

			// Check return value
			if(!method.getReturnType().equals(Response.class)){

				// Build error message
				StringBuilder sb = new StringBuilder();
				sb.append("The return type of method with @Resource annotation in ");
				sb.append(servlet.getName());
				sb.append('.');
				sb.append(method.getName());
				sb.append(" is not ");
				sb.append(Response.class.getName());
				sb.append(".");

				// Throw it
				throw new ResourceMappingException(sb.toString());
			}

			// Check and collect parameters
			Annotation[][] annotations = method.getParameterAnnotations();
			Class[] parameterTypes = method.getParameterTypes();
			String[] parameter_names = new String[parameterTypes.length];
			String[] default_values = new String[parameter_names.length];
			for(int parameter_index = 0; parameter_index < annotations.length; parameter_index++){
				System.out.println("parameter_index: " + parameter_index);

				// Iterate through annotations
				for(int annotation_index = 0; annotation_index < annotations[parameter_index].length; annotation_index++){
					System.out.println(
							"annotation_index: " + annotation_index
							+ "\t annotation: " + annotations[parameter_index][annotation_index]
					);


					// Check parameters that we care about
					if(annotations[parameter_index][annotation_index] instanceof DefaultValue){

						// Check value
						String value = ((DefaultValue)annotations[parameter_index][annotation_index]).value();
						if(value.isEmpty()){

							// Build error message
							StringBuilder sb = new StringBuilder();
							sb.append("The value of @DefaultValue annotation in parameter index ");
							sb.append(parameter_index);
							sb.append(" in ");
							sb.append(servlet.getName());
							sb.append('.');
							sb.append(method.getName());
							sb.append(" is empty.");

							// Throw it
							throw new ParameterMappingException(sb.toString());
						}

						// Check if DefaultValue already exists TODO necessary?
						if(default_values[parameter_index] != null){

							// Build error message
							StringBuilder sb = new StringBuilder();
							sb.append("The multiple @DefaultValue annotations in parameter index ");
							sb.append(parameter_index);
							sb.append(" in ");
							sb.append(servlet.getName());
							sb.append('.');
							sb.append(method.getName());
							sb.append('.');

							// Throw it
							throw new ParameterMappingException(sb.toString());
						}

						// All good
						default_values[parameter_index] = value;

					}else if(annotations[parameter_index][annotation_index] instanceof Parameter){

						// Check value
						String value = ((Parameter)annotations[parameter_index][annotation_index]).value();
						if(value.isEmpty()){

							// Build error message
							StringBuilder sb = new StringBuilder();
							sb.append("The value of @Parameter annotation in parameter index ");
							sb.append(parameter_index);
							sb.append(" in ");
							sb.append(servlet.getName());
							sb.append('.');
							sb.append(method.getName());
							sb.append(" is empty.");

							// Throw it
							throw new ParameterMappingException(sb.toString());
						}

						// Check if Parameter already exists TODO necessary?
						if(parameter_names[parameter_index] != null){

							// Build error message
							StringBuilder sb = new StringBuilder();
							sb.append("The multiple @Parameter annotations in parameter index ");
							sb.append(parameter_index);
							sb.append(" in ");
							sb.append(servlet.getName());
							sb.append('.');
							sb.append(method.getName());
							sb.append('.');

							// Throw it
							throw new ParameterMappingException(sb.toString());
						}

						// All good
						parameter_names[parameter_index] = value;
					}
				}

				// Make sure Parameter exists if DefaultValue is used
				if(parameter_names[parameter_index] == null && default_values[parameter_index] != null){

					// Build error message
					StringBuilder sb = new StringBuilder();
					sb.append("The @Parameter is missing in parameter where @DefaultValue is used. The parameter index is ");
					sb.append(parameter_index);
					sb.append(" in ");
					sb.append(servlet.getName());
					sb.append('.');
					sb.append(method.getName());
					sb.append(". Parameters with @DefaultValue are required to have @Parameter.");

					// Throw it
					throw new ParameterMappingException(sb.toString());
				}
			}

			// Build Resource Function
			ResourceFunction rf = new ResourceFunction(
					resource_string,
					servlet,
					method,
					default_values,
					parameter_names
			);

			// Check if there's duplicate resources within the servlet
			System.out.println(resource_string);
			if(temp_resources.containsKey(resource_string)){

				// Pull that conflicting RF
				ResourceFunction conflicting_rf = temp_resources.get(resource_string);

				// Build error message
				StringBuilder sb = new StringBuilder();
				sb.append("Duplicate resource '");
				sb.append(resource_string);
				sb.append("' is found within the servlet. ");
				sb.append(method.getName());
				sb.append(" and ");
				sb.append(conflicting_rf.getMethod().getName());

				// Throw it
				throw new DuplicateResourceException(sb.toString());
			}

			// Add in map
			System.out.println("Adding in: " + resource_string);
			temp_resources.put(resource_string, rf);
		}

		// Check for conflicts
		for(Map.Entry<String,ResourceFunction> entry : temp_resources.entrySet()){

			// If not set to overwrite, throw exception on duplicate resource names
			if(!overwrite_duplicates && this.resources.containsKey(entry.getKey())){

				// Pull that conflicting RF
				ResourceFunction conflicting_rf = this.resources.get(entry.getKey());

				// Build error message
				StringBuilder sb = new StringBuilder();
				sb.append("Duplicate resource '");
				sb.append(entry.getKey());
				sb.append("' is found. ");
				sb.append("Name of method that holds the new resource: ");
				sb.append(servlet.getName());
				sb.append('.');
				sb.append(entry.getValue().getMethod().getName());
				sb.append(" Name of method that currently holds the resource");
				sb.append(conflicting_rf.getClazz().getName());
				sb.append('.');
				sb.append(conflicting_rf.getMethod().getName());

				// Throw it
				throw new DuplicateResourceException(sb.toString());
			}
		}

		// Merge
		this.resources.putAll(temp_resources);
	}

	Response serviceRequest(final String resource, final DataProtocolRequest request) throws
		ResourceNotFoundException,
		IllegalArgumentException,
		InternalServerErrorException,
		InvalidParametersException
	{

		// Check input
		if(resource == null) throw new IllegalArgumentException("resource is null");
		if(request == null) throw new IllegalArgumentException("response is null");

		// Check if resource exists // TODO regex
		if(!resources.containsKey(resource)) throw new ResourceNotFoundException(resource);

		// Get resource function
		ResourceFunction rf = resources.get(resource);

		// Check required parameters
		Map<String,ParameterErrorType> error = new HashMap<>();
		String[] parameters = new String [rf.getParameterNames().length];
		String[] parameter_names = rf.getParameterNames();
		String[] default_values = rf.getDefaultValues();
		boolean is_error = false;
		for(int i = 0; i < parameters.length; i++){

			// Check if this parameter is required and is supplied
			if(
				parameter_names[i] != null &&
				default_values[i] == null &&
				!request.getParameters().containsKey(parameter_names[i])
			){
				error.put(parameter_names[i], ParameterErrorType.MISSING);
				is_error = true;
				continue;
			}

			// Check if parameter has default value. If so, use default value
			if(parameter_names[i] != null && default_values[i] != null){
				error.put(parameter_names[i], ParameterErrorType.MISSING);
				parameters[i] = default_values[i];
			}

			// Assign value
			if(parameter_names[i] != null && request.getParameters().containsKey(parameter_names[i])){
				parameters[i] = request.getParameters().get(parameter_names[i]);
			}
		}

		// If error has occurred, return the error protocol
		if(is_error) throw new InvalidParametersException(error);

		// Make the response
		try{
			return (Response) rf.getMethod().invoke(default_values);
		}catch(IllegalAccessException | InvocationTargetException e){
			throw new InternalServerErrorException(
				"Failed to invoke method " +rf.getClazz().getName() + "." + rf.getMethod().getName(),
				e
			);
		}
	}

	Response serviceRequest(final NewDataProtocolRequest request) throws
		ResourceNotFoundException,
		IllegalArgumentException,
		InternalServerErrorException,
		InvalidParametersException
	{
		return this.serviceRequest(request.getResource(), request);
	}

	void destroyConnection(final int client_id) throws NoSuchElementException{

		// Check if client id exists
		if(!this.connections.containsKey(client_id)){
			throw new NoSuchElementException("Client id " + client_id + " cannot be found in the connection manager.");
		}

		// Delete it
		this.connections.remove(client_id);
	}
}
