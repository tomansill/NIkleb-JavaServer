package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;

public class IdStatusProtocolResponse extends IdProtocolResponse implements NextIdProtocolResponse{

	protected final Status status;

	protected int next_id;

	IdStatusProtocolResponse(final Type type, final Status status){
		super(type);
		this.status = status;
	}

	IdStatusProtocolResponse(final int id, final Type type, final Status status){
		super(id, type);
		this.status = status;
	}

	public Status getStatus(){
		return this.status;
	}

	public int getNextId(){
		return this.next_id;
	}

	public void setNextId(final int next_id){
		this.next_id = next_id;
	}
}
