package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Type;
import com.tomansill.nikleb.protocol.ProtocolCloseReason;

public class ErrorProtocolResponse extends ProtocolResponse implements CloseOnProtocolResponse{

	public ErrorProtocolResponse(){
		super(Type.MESSAGE_ERROR);
	}

	@Override
	public ProtocolCloseReason getReason(){
		return ProtocolCloseReason.PROTOCOL_ERROR;
	}
}
