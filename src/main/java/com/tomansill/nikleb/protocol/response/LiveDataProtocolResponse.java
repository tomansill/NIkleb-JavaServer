package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;

public class LiveDataProtocolResponse extends IdStatusProtocolResponse implements NextIdProtocolResponse{

	private final String data;

	public LiveDataProtocolResponse(final int id, final String data) throws IllegalArgumentException{
		super(id, Type.REQUEST_STREAM, Status.ACCEPTED);
		if(data == null) throw new IllegalArgumentException("data is null");
		this.data = data;
	}
}
