package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/** Handshake Success Message */
public class HandshakeSuccessProtocolResponse extends ProtocolResponse implements NextIdProtocolResponse{

	/** Status */
	private final Status status = Status.ACCEPTED;

	/** List of approved options */
	private final Set<String> accepted_options = new HashSet<>();

	/** Next_id */
	private int next_id;

	/** Default constructor */
	public HandshakeSuccessProtocolResponse(){
		super(Type.HANDSHAKE);
	}

	/** Adds accepted option to the list of accepted options. Any duplicate options are automatically ignored.
	 *  @param option option
	 *  @throws IllegalArgumentException thrown if option is not valid
	 */
	public void addAcceptedOption(final String option) throws IllegalArgumentException{
		if(option == null || option.isEmpty()) throw new IllegalArgumentException("option is null");
		this.accepted_options.add(option);
	}

	/** Adds accepted options to the list of accepted options. Any duplicate options are automatically ignored.
	 *  @param options collection of option
	 *  @throws IllegalArgumentException thrown if any one of options is not valid
	 */
	public void addAcceptedOption(final Collection<String> options) throws IllegalArgumentException{
		for(final String option : options) this.addAcceptedOption(option);
	}

	@Override
	public int getNextId(){
		return this.next_id;
	}

	@Override
	public void setNextId(int next_id){
		this.next_id = next_id;
	}
}