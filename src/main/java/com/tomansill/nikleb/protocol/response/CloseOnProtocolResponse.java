package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.protocol.ProtocolCloseReason;

public interface CloseOnProtocolResponse{
	public ProtocolCloseReason getReason();
}
