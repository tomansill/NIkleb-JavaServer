package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;

public class RequestFailureInternalServerError extends IdStatusProtocolResponse{


	private final String message;

	public RequestFailureInternalServerError(final int id, final Type type, final String message) throws IllegalArgumentException{
		super(id, type, Status.INTERNAL_SERVER_ERROR);
		if(message == null) throw new IllegalArgumentException("message is null");
		this.message = message;
	}
}
