package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Type;

public abstract class IdProtocolResponse extends ProtocolResponse{

	protected int id;

	public IdProtocolResponse(Type type){
		super(type);
	}

	public IdProtocolResponse(int id, Type type){
		super(type);
		this.id = id;
	}

	public int getId(){
		return this.id;
	}

	public void setId(final int id){
		this.id = id;
	}
}
