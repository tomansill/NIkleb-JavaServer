package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Type;

public class LiveDataUpdateProtocolResponse extends IdProtocolResponse implements NextIdProtocolResponse{

	private final String data;

	private int next_id;

	public LiveDataUpdateProtocolResponse(final int id, final String data) throws IllegalArgumentException{
		super(id, Type.REQUEST_STREAM);
		if(data == null) throw new IllegalArgumentException("data is null");
		this.data = data;
	}

	@Override
	public int getNextId(){
		return this.next_id;
	}

	@Override
	public void setNextId(int next_id){
		this.next_id = next_id;
	}
}
