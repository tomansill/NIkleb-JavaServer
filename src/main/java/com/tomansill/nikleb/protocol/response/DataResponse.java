package com.tomansill.nikleb.protocol.response;

public interface DataResponse{

	public void update(String data);

	public void terminate();

}
