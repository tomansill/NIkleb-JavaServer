package com.tomansill.nikleb.protocol.response;

public interface NextIdProtocolResponse{

	public int getNextId();

	public void setNextId(final int next_id);
}
