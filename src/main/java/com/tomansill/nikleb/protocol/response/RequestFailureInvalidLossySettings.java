package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.ParameterErrorType;
import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;

import java.util.HashMap;
import java.util.Map;

public class RequestFailureInvalidLossySettings extends IdStatusProtocolResponse{

	private final Map<String, ParameterErrorType> settings = new HashMap<>();

	public RequestFailureInvalidLossySettings(final int id, final Type type, final Map<String, ParameterErrorType> settings) throws IllegalArgumentException{
		super(id, type, Status.ERROR_INVALID_LOSSY_SETTINGS);

		if(settings == null) throw new IllegalArgumentException("settings is null");

		for(final Map.Entry<String, ParameterErrorType> setting : settings.entrySet()){
			if(setting.getKey().isEmpty()) throw new IllegalArgumentException("empty setting name in the map!");
			if(setting.getValue() == null) throw new IllegalArgumentException("setting name '" + setting.getKey() + "' has a null value!");
		}

		this.settings.putAll(settings);
	}
}
