package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;

public class RequestFailureResourceNotFound extends IdStatusProtocolResponse implements NextIdProtocolResponse{
	public RequestFailureResourceNotFound(final int id, final Type type) throws IllegalArgumentException{
		super(id, type, Status.ERROR_RESOURCE_NOT_FOUND);
	}
}
