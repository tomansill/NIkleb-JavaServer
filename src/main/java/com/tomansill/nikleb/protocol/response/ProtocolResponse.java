package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Type;

public abstract class ProtocolResponse{

	protected Type type;

	public ProtocolResponse(final Type type){
		if(type == null) throw new IllegalArgumentException("Type Enum is null");
		this.type = type;
	}

	public Type getType(){
		return this.type;
	}
}
