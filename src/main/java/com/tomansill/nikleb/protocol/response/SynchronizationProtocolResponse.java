package com.tomansill.nikleb.protocol.response;

import static com.tomansill.nikleb.Enum.Type.SYNCHRONIZATION;

public class SynchronizationProtocolResponse extends ProtocolResponse implements NextIdProtocolResponse{

	private int next_id;

	public SynchronizationProtocolResponse(){
		super(SYNCHRONIZATION);
	}

	@Override
	public int getNextId(){
		return 0;
	}

	@Override
	public void setNextId(int next_id){
		this.next_id = next_id;
	}
}
