package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;

public class RequestFailureException extends IdStatusProtocolResponse{

	private final Object message;

	public RequestFailureException(final int id, final Type type, final Object message) throws IllegalArgumentException{
		super(id, type, Status.EXCEPTION);
		if(message == null) throw new IllegalArgumentException("message is null");
		this.message = message;
	}
}