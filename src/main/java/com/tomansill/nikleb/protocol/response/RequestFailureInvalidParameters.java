package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.ParameterErrorType;
import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;

import java.util.HashMap;
import java.util.Map;

public class RequestFailureInvalidParameters extends IdStatusProtocolResponse{

	private final Map<String, ParameterErrorType> parameters = new HashMap<>();

	public RequestFailureInvalidParameters(final int id, final Type type, final Map<String, ParameterErrorType> parameters) throws IllegalArgumentException{
		super(id, type, Status.ERROR_INVALID_PARAMETERS);
		if(parameters == null) throw new IllegalArgumentException("parameters is null");
		for(final Map.Entry<String, ParameterErrorType> parameter : parameters.entrySet()){
			if(parameter.getKey().isEmpty()) throw new IllegalArgumentException("empty parameter name in the map!");
			if(parameter.getValue() == null) throw new IllegalArgumentException("parameter name '" + parameter.getKey() + "' has a null value!");
		}
		this.parameters.putAll(parameters);
	}
}
