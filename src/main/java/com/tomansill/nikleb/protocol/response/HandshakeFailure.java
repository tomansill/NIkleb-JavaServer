package com.tomansill.nikleb.protocol.response;

import com.tomansill.nikleb.Enum.Code;
import com.tomansill.nikleb.Enum.Status;
import com.tomansill.nikleb.Enum.Type;
import com.tomansill.nikleb.Nikleb;

import java.util.Set;

/** Handshake Failure Message */
public class HandshakeFailure extends ProtocolResponse{

	/** Status */
	private final Status status = Status.REJECTED;

	/** Code */
	private final Code code = Code.PROTOCOL_VERSION_NOT_SUPPORTED;

	/** List of supported protocols */
	private final Set<String> supported = Nikleb.getSupportedProtocolVersions();

	/** Default constructor */
	public HandshakeFailure(){
		super(Type.HANDSHAKE);
	}
}
