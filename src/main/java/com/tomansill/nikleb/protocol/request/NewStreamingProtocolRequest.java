package com.tomansill.nikleb.protocol.request;

import com.tomansill.nikleb.Enum.Type;

import java.util.Map;

public class NewStreamingProtocolRequest extends NewDataProtocolRequest implements StreamingProtocolRequest{

	public NewStreamingProtocolRequest(final Type type, final int id, final String t, final Map<String,String> parameters, final String resource){
		super(type, id, t, parameters, resource);
	}

	@Override // TODO lossy option
	public Object getLossySettings(){
		return null;
	}
}
