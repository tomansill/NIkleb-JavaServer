package com.tomansill.nikleb.protocol.request;

import com.tomansill.nikleb.Enum.Type;

public abstract class ProtocolRequest{

	private final Type type;

	public ProtocolRequest(final Type type){
		this.type = type;
	}

	public Type getType(){
		return this.type;
	}

}
