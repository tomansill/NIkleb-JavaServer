package com.tomansill.nikleb.protocol.request;

public interface StreamingProtocolRequest{

	public Object getLossySettings();
}
