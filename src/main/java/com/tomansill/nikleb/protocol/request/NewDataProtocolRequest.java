package com.tomansill.nikleb.protocol.request;

import com.tomansill.nikleb.Enum.Type;

import java.util.Map;

public abstract class NewDataProtocolRequest extends DataProtocolRequest{

	private String resource;

	public NewDataProtocolRequest(final Type type, final int id, final String t, final Map<String,String> parameters, final String resource){
		super(type, id, parameters);
		this.resource = resource;
	}

	public String getResource(){
		return this.resource;
	}
}
