package com.tomansill.nikleb.protocol.request;

import com.tomansill.nikleb.Enum.Type;

import java.util.Map;

public class UpdateStreamingProtocolRequest extends DataProtocolRequest implements StreamingProtocolRequest{

	public UpdateStreamingProtocolRequest(final Type type, final int id, final Map<String,String> parameters){
		super(type, id, parameters);
	}

	@Override // TODO lossy stuff
	public Object getLossySettings(){
		return null;
	}
}
