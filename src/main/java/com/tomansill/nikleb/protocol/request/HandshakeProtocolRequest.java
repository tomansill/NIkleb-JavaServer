package com.tomansill.nikleb.protocol.request;

import com.tomansill.nikleb.Enum.Type;

import java.util.ArrayList;

public class HandshakeProtocolRequest extends ProtocolRequest{

	private String protocol_version;

	private ArrayList<String> options;

	public HandshakeProtocolRequest(final String protocol_version, final ArrayList<String> options){
		super(Type.HANDSHAKE);
		this.protocol_version = protocol_version;
		this.options = new ArrayList<>(options);
	}

	public String getProtocolVersion(){
		return this.protocol_version;
	}

	public ArrayList<String> getOptions(){
		return this.options;
	}
}
