package com.tomansill.nikleb.protocol.request;

import com.tomansill.nikleb.Enum.Type;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class DataProtocolRequest extends ProtocolRequest{
	
	private int id;

	private Map<String,String> parameters;

	public DataProtocolRequest(final Type type, final int id, final Map<String,String> parameters){
		super(type);
		this.id = id;
		this.parameters = new HashMap<>(parameters);
	}

	public int getId(){
		return id;
	}


	public Map<String, String> getParameters(){
		return Collections.unmodifiableMap(this.parameters);
	}
}
