package com.tomansill.nikleb.protocol.request;

import com.tomansill.nikleb.Enum.Type;

import java.util.Map;

public class SingleFireProtocolRequest extends NewDataProtocolRequest{

	public SingleFireProtocolRequest(final Type type, final int id, final String t, final Map<String,String> parameters, final String resource){
		super(type, id, t, parameters, resource);
	}
}
