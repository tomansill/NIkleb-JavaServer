package com.tomansill.nikleb.protocol;

public enum ProtocolCloseReason{
	TIMEOUT,
	INTERNAL_SERVER_ERROR,
	PROTOCOL_ERROR,
	NORMAL;
}
