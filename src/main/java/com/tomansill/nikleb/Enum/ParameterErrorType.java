package com.tomansill.nikleb.Enum;

public enum ParameterErrorType{
	OK,
	MISSING,
	UNKNOWN,
	OPTIONAL,
	INVALID
}
