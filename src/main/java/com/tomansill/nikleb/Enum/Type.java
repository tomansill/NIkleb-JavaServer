package com.tomansill.nikleb.Enum;

import java.util.HashMap;
import java.util.Map;

public enum Type{
	HANDSHAKE,
	SYNCHRONIZATION,
	MESSAGE_ERROR,
	REQUEST_SINGLE,
	REQUEST_STREAM,
	STREAM_UPDATE,
	STREAM_TERMINATE,
	REQUEST_STREAM_UPDATE,
	REQUEST_STREAM_SETTINGS,
	REQUEST_STREAM_TERMINATE,
	SETTINGS_UPDATE;

	/* Map of String to Enum */
	private static Map<String,Type> TYPE_MAP = new HashMap<>();

	/* Initializes the map */
	static{
		for(Type type : Type.values()) TYPE_MAP.put(type.toString(), type);
	}

	/** Returns corresponding type to the input string
	 *  @param type_string type in string format
	 *  @return type or null if it doesn't exist
	 */
	public static Type get(final String type_string){
		return TYPE_MAP.get(type_string);
	}
}
