package com.tomansill.nikleb.response;

public enum ResponseType{
	OK,
	ERROR,
	SERVER_ERROR
}
