package com.tomansill.nikleb.response;

public class ResponseBuilder{

	/*#################### STATIC MEMBERS ####################*/

	public static DataResponseBuilder ok(){
		return new DataResponseBuilder();
	}

	public static ErrorResponseBuilder serverError(String message){
		return new ErrorResponseBuilder(message, true);
	}

	public static ErrorResponseBuilder serverError(Throwable throwable){
		return new ErrorResponseBuilder(throwable, true);
	}

	public static ErrorResponseBuilder error(String message){
		return new ErrorResponseBuilder(message, false);
	}

	public static ErrorResponseBuilder error(Throwable throwable){
		return new ErrorResponseBuilder(throwable, false);
	}

	/*#################### CLASS MEMBERS ####################*/

	private ResponseBuilder(){}

	/*#################### STATIC CLASSES ####################*/

	/** Data Response Builder for building Data Responses */
	public static class DataResponseBuilder extends ResponseBuilder{

		/** Data container */
		private Data data;

		/** Flag to indicate that this data is single-fire (chosen by the developer, not client) */
		private boolean single_data = false;

		/** Close function */
		private Runnable close_function;

		/** Private constructor */
		private DataResponseBuilder(){}

		/** Adds the initial data in the data response and send it to the client. <b>NOTE:</b> This is a fire-and-forget
		 *  version of data() method where you can't further update the data when the Response is built.
		 *  @param data data
		 *  @return DataResponseBuilder object
		 */
		public DataResponseBuilder data(String data){
			this.data = new Data();
			single_data = true;
			this.data.update(data);
			return this;
		}

		/** Links the data container to this the response. The Data container becomes useable after this method.
		 *  @param data data container
		 *  @return DataResponseBuilder object
		 */
		public DataResponseBuilder data(Data data){
			return data(data, null);
		}

		/** Links the data container to this the response. The Data container becomes useable after this method.
		 *  @param data data container
		 *  @param initial_data initial data
		 *  @return DataResponseBuilder object
		 */
		public DataResponseBuilder data(Data data, String initial_data) throws IllegalArgumentException{

			// Make sure data is not null
			if(data == null) throw new IllegalArgumentException("data is null");

			// Set data
			this.data = data;

			// If initial data is not null, prime the data with initial data
			if(initial_data!= null) this.data.update(initial_data);

			// Return itself
			return this;
		}

		/** Links the function that will be fired when the client is finished with the data. Used when a clean up is needed.
		 *  @param close_function Runnable function that is fired when client closes the data
		 *  @return DataResponseBuilder object
		 */
		public DataResponseBuilder onClose(final Runnable close_function){
			this.close_function = close_function;
			return this;
		}

		/** Builds the response
		 *  @return response
		 *  @throws IllegalStateException thrown if the ResponseBuilder is in invalid state
		 */
		public Response build() throws IllegalStateException{

			// Check data()
			if(this.data == null) throw new IllegalStateException("data() was never called. data() must be called before building the Response.");

			// Build Response
			return new DataResponse(this.data, this.close_function, this.single_data);
		}
	}

	public static class ErrorResponseBuilder extends ResponseBuilder{

		private boolean server_error;

		private String message;

		private Throwable throwable;

		private ErrorResponseBuilder(final String message, final boolean server_error){
			this.server_error = server_error;
			this.message = message;
		}

		private ErrorResponseBuilder(final Throwable throwable, final boolean server_error){
			this.server_error = server_error;
			this.throwable = throwable;
		}

		/** Builds the response
		 *  @return response
		 */
		public Response build(){

			// Build Response
			if(throwable != null) return new ErrorResponse(this.server_error, throwable);
			else return new ErrorResponse(this.server_error, message);
		}
	}
}
