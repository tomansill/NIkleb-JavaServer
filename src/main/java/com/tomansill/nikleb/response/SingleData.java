package com.tomansill.nikleb.response;

import com.tomansill.nikleb.protocol.response.ProtocolResponse;
import com.tomansill.nikleb.protocol.response.SingleDataProtocolResponse;

public class SingleData extends AbstractData{

	SingleData(){
		super();
	}

	SingleData(final Runnable close_function){
		super(close_function);
	}

	@Override
	public void update(final String data){
		super.update(data);
		this.close();
	}

	@Override
	public ProtocolResponse convertToResponse(final String data){
		return new SingleDataProtocolResponse(this.getId(), data);
	}
}
