package com.tomansill.nikleb.response;

import com.tomansill.nikleb.protocol.response.ProtocolResponse;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Consumer;

public abstract class AbstractData implements AutoCloseable{

	/** Close function to be run on server side when client closes the stream */
	private Runnable close_function;

	/** ProtocolRequest id */
	private int request_id;

	/** Output stream */
	protected Consumer<ProtocolResponse> output;

	/** Buffer */
	protected final Queue<String> buffer = new LinkedList<>();

	/** Flag to indicate that data is closed */
	private boolean is_closed = false;

	/** Default Constructor */
	protected AbstractData(){
		this.close_function = null;
	}

	/** Constructor that takes in close function
	 *  @param close_function close function for server to exit when the client has closed on this data
	 */
	protected AbstractData(final Runnable close_function){
		this.close_function = close_function;
	}

	/** Sets the output stream
	 *  @param output callable where the ProtocolResponse object will be passed onto
	 */
	synchronized void setOutputStream(final int request_id, final Consumer<ProtocolResponse> output){

		// If already closed, then just ignore
		if(this.is_closed) return;

		// Set the response id
		this.request_id = request_id;

		// Set the output feed
		this.output = output;

		// If user has intentionally removed the previous output function, then just exit here
		if(output == null) return;

		// If there's some data on the buffer, go convertToResponse all
		String data;
		while((data = buffer.poll()) != null){
			this.convertToResponse(data);
		}
	}

	/** Updates the data
	 *  @param data new data
	 */
	public synchronized void update(String data){

		// If already closed, ignore
		if(this.is_closed) return;

		// If output is not set up, store data in the buffer
		if(this.output == null) buffer.add(data);

		// If output is set up, convert it to protocol then send it
		else this.output.accept(convertToResponse(data));
	}

	/** Wraps the data into ProtocolResponse
	 *  @param data data string
	 */
	protected abstract ProtocolResponse convertToResponse(String data);

	/** Commands the stream to close. */
	public synchronized void close(){
		this.close(true);
	}

	/** Commands the stream to close.
	 *  @param run_close_function true to run the supplied close function to allow the server to clean up, otherwise false
	 */
	public synchronized void close(final boolean run_close_function){

		// If already closed, then just ignore
		if(this.is_closed) return;

		// Otherwise set flag
		this.is_closed = true;

		// Run the close function to allow the server to clean up
		if(run_close_function && this.close_function != null) this.close_function.run();
	}

	/** Returns close status
	 *  @return true if this data is closed, otherwise false
	 */
	public boolean isClosed(){
		return this.is_closed;
	}

	/** Returns response id
	 *  @return response id
	 */
	protected int getId(){
		return this.request_id;
	}
}
