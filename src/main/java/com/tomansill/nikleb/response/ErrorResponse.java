package com.tomansill.nikleb.response;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class ErrorResponse extends Response{

	private String message;

	ErrorResponse(final boolean server_error, final String message){
		super(server_error ? ResponseType.SERVER_ERROR : ResponseType.ERROR);
		this.message = message;
	}

	ErrorResponse(final boolean server_error, final Throwable throwable){
		super(server_error ? ResponseType.SERVER_ERROR : ResponseType.ERROR);

		// Collect full error message
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try(PrintStream ps = new PrintStream(baos, true, StandardCharsets.UTF_8.name())){
			throwable.printStackTrace(ps);
			this.message = new String(baos.toByteArray(), StandardCharsets.UTF_8);
		}catch(UnsupportedEncodingException e){
			e.printStackTrace();
			this.message = throwable.toString();
		}
	}
}
