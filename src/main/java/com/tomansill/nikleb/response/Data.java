package com.tomansill.nikleb.response;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Data{

	private AbstractData data;

	private Lock lock = new ReentrantReadWriteLock(true).writeLock();

	private Queue<String> buffer = new ConcurrentLinkedQueue<>();

	public Data(){
		this.lock.lock(); // Lock until setData is called
	}

	void setData(final AbstractData data){
		this.data = data;

		// Pop off buffer if any
		String buf_data;
		while((buf_data = this.buffer.poll()) != null) this.data.update(buf_data);

		// Unlock
		this.lock.unlock();
	}

	public void update(String data){
		if(data == null) return;
		if(this.data != null){
			this.lock.lock();
			try{
				this.data.update(data);
			}finally{
				this.lock.unlock();
			}
		}else{
			buffer.offer(data);
		}
	}

	public boolean isClosed(){
		this.lock.lock();
		try{
			return this.data.isClosed();
		}finally{
			this.lock.unlock();
		}
	}

	public void close(){
		this.lock.lock();
		try{
			this.data.close();
		}finally{
			this.lock.unlock();
		}
	}

	public void close(final boolean run_close_function){
		this.lock.lock();
		try{
			this.data.close(run_close_function);
		}finally{
			this.lock.unlock();
		}
	}
}