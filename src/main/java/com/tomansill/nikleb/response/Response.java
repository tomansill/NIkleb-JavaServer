package com.tomansill.nikleb.response;

public abstract class Response{

	private final ResponseType type;

	public Response(ResponseType type){
		this.type = type;
	}

	public ResponseType getType(){
		return this.type;
	}
}
