package com.tomansill.nikleb.response;

import com.tomansill.nikleb.protocol.response.LiveDataProtocolResponse;
import com.tomansill.nikleb.protocol.response.LiveDataUpdateProtocolResponse;
import com.tomansill.nikleb.protocol.response.ProtocolResponse;

public class LiveData extends AbstractData{

	private boolean first_time = true;

	LiveData(final String initial_data){
		super();
		this.update(initial_data);
	}

	LiveData(final Runnable close_function, final String initial_data){
		super(close_function);
		this.update(initial_data);
	}

	@Override
	public ProtocolResponse convertToResponse(final String data){

		// If first time, send first-time message with nextid
		if(first_time){
			first_time = false;
			return new LiveDataProtocolResponse(this.getId(), data);
		}

		return new LiveDataUpdateProtocolResponse(this.getId(), data);
	}
}
