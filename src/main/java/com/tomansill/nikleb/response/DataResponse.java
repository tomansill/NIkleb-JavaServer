package com.tomansill.nikleb.response;

public class DataResponse extends Response{

	private final Data data_container;

	private final Runnable close_function;

	private final boolean single_data;

	DataResponse(final Data data_container, final Runnable close_function, final boolean single_data){
		super(ResponseType.OK);
		this.data_container = data_container;
		this.close_function = close_function;
		this.single_data =single_data;
	}

	public Data getDataContainer(){
		return data_container;
	}

	public Runnable getCloseFunction(){
		return close_function;
	}

	public boolean isSingleData(){
		return single_data;
	}
}
