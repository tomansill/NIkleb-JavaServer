package com.tomansill.nikleb.exception;

public class ResourceMappingException extends RuntimeException{

	public ResourceMappingException(final String message){
		super(message);
	}

	public ResourceMappingException(final String message, final Throwable throwable){
		super(message, throwable);
	}

	public ResourceMappingException(final Throwable throwable){
		super(throwable);
	}
}
