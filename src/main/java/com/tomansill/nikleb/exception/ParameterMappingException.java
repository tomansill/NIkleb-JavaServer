package com.tomansill.nikleb.exception;

public class ParameterMappingException extends RuntimeException{

	public ParameterMappingException(final String message){
		super(message);
	}

	public ParameterMappingException(final String message, final Throwable throwable){
		super(message, throwable);
	}

	public ParameterMappingException(final Throwable throwable){
		super(throwable);
	}
}
