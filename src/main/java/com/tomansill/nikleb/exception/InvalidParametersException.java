package com.tomansill.nikleb.exception;

import com.tomansill.nikleb.Enum.ParameterErrorType;

import java.util.Map;

public class InvalidParametersException extends Exception{

	private final Map<String,ParameterErrorType> errors;

	public InvalidParametersException(Map<String,ParameterErrorType> errors){
		super("Parameters are invalid");
		this.errors = errors;
	}
}
