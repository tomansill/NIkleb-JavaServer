package com.tomansill.nikleb.exception;

public class ResourceNotFoundException extends Exception{

	public ResourceNotFoundException(String resource){
		super("Resource `" + resource + "` cannot be found on the service");
	}
}
