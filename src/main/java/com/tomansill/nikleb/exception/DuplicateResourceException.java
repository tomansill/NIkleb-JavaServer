package com.tomansill.nikleb.exception;

public class DuplicateResourceException extends RuntimeException{
	public DuplicateResourceException(final String message){
		super(message);
	}

	public DuplicateResourceException(final String message, final Throwable throwable){
		super(message, throwable);
	}

	public DuplicateResourceException(final Throwable throwable){
		super(throwable);
	}
}
