package com.tomansill.nikleb.exception;

public class InternalServerErrorException extends RuntimeException{

	public InternalServerErrorException(final String message){
		super(message);
	}

	public InternalServerErrorException(final String message, final Throwable throwable){
		super(message, throwable);
	}

	public InternalServerErrorException(final Throwable throwable){
		super(throwable);
	}
}
