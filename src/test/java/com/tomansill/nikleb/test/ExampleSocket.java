package com.tomansill.nikleb.test;

import com.google.gson.Gson;
import com.tomansill.nikleb.Connection;
import com.tomansill.nikleb.Nikleb;
import com.tomansill.nikleb.protocol.ProtocolCloseReason;
import com.tomansill.nikleb.protocol.response.ProtocolResponse;

import javax.websocket.*;
import javax.websocket.server.ServerEndpointConfig;
import java.io.IOException;

public class ExampleSocket extends Endpoint{

	private final Connection connection;

	private Session session = null;

	public ExampleSocket(final Nikleb context){
		this.connection = context.createConnection(
				this::sendSyncMessage,
				this::sendAsyncMessage,
				this::closeConnection
		);
	}

	@Override
	public void onOpen(Session session, EndpointConfig endpointConfig){

		// Get session object
		this.session = session;

		// Handle messages
		session.addMessageHandler(new MessageHandler.Whole<String>(){
			@Override
			public void onMessage(final String message){
				connection.process(message);
			}
		});
	}

	@Override
	public void onClose(Session session, CloseReason closeReason){

	}

	@Override
	public void onError(Session session, Throwable throwable){

	}

	private void sendSyncMessage(final ProtocolResponse response){
		this.sendSyncMessage(new Gson().toJson(response));
	}

	private void sendAsyncMessage(final ProtocolResponse response){
		this.sendAsyncMessage(new Gson().toJson(response));
	}

	private void sendSyncMessage(final String message){
		if(session != null){
			try{
				session.getBasicRemote().sendText(message);
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	private void sendAsyncMessage(final String message){
		if(session != null) session.getAsyncRemote().sendText(message);
	}

	private void closeConnection(ProtocolCloseReason cr){
		if(session != null && cr != null){
			try{
				switch(cr){
					case TIMEOUT:
						session.close(new CloseReason(CloseReason.CloseCodes.GOING_AWAY, "Timed out"));
						break;
					case PROTOCOL_ERROR:
						session.close(new CloseReason(CloseReason.CloseCodes.PROTOCOL_ERROR, "Protocol Error"));
						break;
					case INTERNAL_SERVER_ERROR:
						session.close(new CloseReason(CloseReason.CloseCodes.UNEXPECTED_CONDITION, "An internal server error has occurred."));
						break;
					case NORMAL:
						session.close(new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE, ""));
						break;
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	public static class Configurator extends ServerEndpointConfig.Configurator{
		private final Nikleb nikleb;

		public Configurator(final Nikleb servlet) {
			this.nikleb = servlet;
		}

		@SuppressWarnings("unchecked")
		public <T> T getEndpointInstance(final Class<T> clazz) throws InstantiationException {
			return (T)new ExampleSocket(this.nikleb);
		}
	}
}