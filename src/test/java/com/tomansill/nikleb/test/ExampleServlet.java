package com.tomansill.nikleb.test;

import com.tomansill.nikleb.annotation.DefaultValue;
import com.tomansill.nikleb.annotation.Parameter;
import com.tomansill.nikleb.annotation.Resource;
import com.tomansill.nikleb.response.Data;
import com.tomansill.nikleb.response.Response;
import com.tomansill.nikleb.response.ResponseBuilder;

@Resource("")
public class ExampleServlet{

	@Resource("connection_count")
	public Response getConnectionCount(){
		return null;
	}

	@Resource("get_temperature")
	public Response getTemperature(
		@Parameter("name") final String name,
		@Parameter("unit") @DefaultValue("C") final String unit,
		String dummy_value
	){
		Data data = new Data();

		return ResponseBuilder
			.ok()
			.data(
				data,
				"30"
			)
			.onClose(() -> System.out.println("close!"))
			.build();
	}
}
