package com.tomansill.nikleb.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

public class TestExampleServer{

	private ExampleServer server;

	@Before
	public void setUp(){
		server = new ExampleServer(8080);
		new Thread(server).start();
	}

	@After
	public void tearDown() throws Exception{
		if(server != null) server.stop();
	}

	@Test
	public void runServerIndefinitely() throws InterruptedException{

		// Create a barrier
		final CountDownLatch cdl = new CountDownLatch(1);

		// CTRL+C catcher
		Runtime.getRuntime().addShutdownHook(new Thread(cdl::countDown));

		// Print ready
		System.out.println("READY!");

		// Wait until CTRL+C is hit
		cdl.await();
	}
}
