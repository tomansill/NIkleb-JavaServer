package com.tomansill.nikleb.test;

import com.tomansill.nikleb.Nikleb;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;

import javax.websocket.server.ServerContainer;
import javax.websocket.server.ServerEndpointConfig;

public class ExampleServer implements Runnable{

	private Server server;
	Nikleb servlet = new Nikleb();

	public ExampleServer(int port){
		server = new Server();
		ServerConnector connector = new ServerConnector(server);
		connector.setPort(port);
		server.addConnector(connector);

		// Setup the basic application "context" for this application at "/"
		// This is also known as the handler tree (in jetty speak)
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		server.setHandler(context);

		servlet.registerServlet(ExampleServlet.class);

		try{
			// Initialize javax.websocket layer
			ServerContainer wscontainer = WebSocketServerContainerInitializer.configureContext(context);

			// Add WebSocket endpoint to javax.websocket layer
			wscontainer.addEndpoint(EventSocket.class);
			wscontainer.addEndpoint(ServerEndpointConfig.Builder.create(ExampleSocket.class, "/ws").configurator(new ExampleSocket.Configurator(servlet)).build());
		}catch (Throwable t){
			t.printStackTrace(System.err);
		}
	}

	public void start() throws Exception{
		server.start();
	}

	public void run(){
		try{
			server.start();
			server.dump(System.err);
			server.join();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void stop() throws Exception{
		server.stop();
	}
}
